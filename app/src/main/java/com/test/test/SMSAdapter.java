package com.test.test;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.test.test.main.SMSBean;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SMSAdapter extends RecyclerView.Adapter<SMSAdapter.SMSVIewHolder> {

    private SMSBean bean;
    private long smsTime;
    private Context context;

    public SMSAdapter(SMSBean bean, long smsTime) {
        this.bean = bean;
        this.smsTime = smsTime;
    }

    @NonNull
    @Override
    public SMSVIewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_sms, viewGroup, false);
        return new SMSVIewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SMSVIewHolder smsvIewHolder, int i) {
        String smsAddress = bean.getMsgAddress().get(i);
        String smsBody = bean.getMsgBody().get(i);

        if (smsTime == bean.getMsgTime().get(i)) {
            smsvIewHolder.smsAddress.setTextColor(context.getResources().getColor(R.color.colorAccent));
            smsvIewHolder.smsBody.setTextColor(context.getResources().getColor(R.color.colorAccent));
        }

        if (AppUtils.isNotEmpty(smsAddress)) {
            smsvIewHolder.smsAddress.setText(smsAddress);
        } else {
            smsvIewHolder.smsAddress.setVisibility(View.GONE);
        }

        if (AppUtils.isNotEmpty(smsBody)) {
            smsvIewHolder.smsBody.setText(smsBody);
        }
    }

    @Override
    public int getItemCount() {
        return bean.getMsgBody().size();
    }

    class SMSVIewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.sms_address)
        TextView smsAddress;

        @BindView(R.id.sms_body)
        TextView smsBody;

        SMSVIewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
