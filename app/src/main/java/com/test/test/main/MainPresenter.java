package com.test.test.main;

import android.database.Cursor;
import android.net.Uri;

public class MainPresenter implements MainContract.Presenter {

    private MainContract.View view;

    private static final long HOUR_FROM_MILLIS = 60 * 60 * 1000;

    MainPresenter(MainContract.View view) {
        this.view = view;
    }

    @Override
    public void getSMS() {
        Cursor cursor = view.getContext().getContentResolver().query(Uri.parse("content://sms/inbox"), null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            SMSBean bean0 = new SMSBean();
            SMSBean bean1 = new SMSBean();
            SMSBean bean2 = new SMSBean();
            SMSBean bean3 = new SMSBean();
            SMSBean bean6 = new SMSBean();
            SMSBean bean12 = new SMSBean();
            SMSBean bean1Day = new SMSBean();

            do {
                long currentTimeMillis = System.currentTimeMillis();
                String msgBody = cursor.getString(cursor.getColumnIndex("body")).trim();
                String msgAddress = cursor.getString(cursor.getColumnIndex("address")).trim();
                long msgSentDate = Long.parseLong(cursor.getString(cursor.getColumnIndex("date_sent")));

                long timeDiff = currentTimeMillis - msgSentDate;
                if (timeDiff < HOUR_FROM_MILLIS) {
                    bean0.getMsgAddress().add(msgAddress);
                    bean0.getMsgBody().add(msgBody);
                    bean0.getMsgTime().add(msgSentDate);
                } else if (timeDiff > HOUR_FROM_MILLIS && timeDiff < 2 * HOUR_FROM_MILLIS) {
                    bean1.getMsgAddress().add(msgAddress);
                    bean1.getMsgBody().add(msgBody);
                    bean1.getMsgTime().add(msgSentDate);
                } else if (timeDiff > 2 * HOUR_FROM_MILLIS && timeDiff < 3 * HOUR_FROM_MILLIS) {
                    bean2.getMsgAddress().add(msgAddress);
                    bean2.getMsgBody().add(msgBody);
                    bean2.getMsgTime().add(msgSentDate);
                } else if (timeDiff > 3 * HOUR_FROM_MILLIS && timeDiff < 6 * HOUR_FROM_MILLIS) {
                    bean3.getMsgAddress().add(msgAddress);
                    bean3.getMsgBody().add(msgBody);
                    bean3.getMsgTime().add(msgSentDate);
                } else if (timeDiff > 6 * HOUR_FROM_MILLIS && timeDiff < 12 * HOUR_FROM_MILLIS) {
                    bean6.getMsgAddress().add(msgAddress);
                    bean6.getMsgBody().add(msgBody);
                    bean6.getMsgTime().add(msgSentDate);
                } else if (timeDiff > 12 * HOUR_FROM_MILLIS && timeDiff < 24 * HOUR_FROM_MILLIS) {
                    bean12.getMsgAddress().add(msgAddress);
                    bean12.getMsgBody().add(msgBody);
                    bean12.getMsgTime().add(msgSentDate);
                } else if (timeDiff > 24 * HOUR_FROM_MILLIS && timeDiff < 25 * HOUR_FROM_MILLIS) {
                    bean1Day.getMsgAddress().add(msgAddress);
                    bean1Day.getMsgBody().add(msgBody);
                    bean1Day.getMsgTime().add(msgSentDate);
                }

            } while (cursor.moveToNext());

            cursor.close();

            if (!bean0.getMsgBody().isEmpty()) {
                view.setUpSMS("0 hour Ago", bean0);
            }

            if (!bean1.getMsgBody().isEmpty()) {
                view.setUpSMS("1 hour Ago", bean1);
            }

            if (!bean2.getMsgBody().isEmpty()) {
                view.setUpSMS("2 hour Ago", bean2);
            }

            if (!bean3.getMsgBody().isEmpty()) {
                view.setUpSMS("3 hour Ago", bean3);
            }

            if (!bean6.getMsgBody().isEmpty()) {
                view.setUpSMS("6 hour Ago", bean6);
            }

            if (!bean12.getMsgBody().isEmpty()) {
                view.setUpSMS("12 hour Ago", bean12);
            }

            if (!bean1Day.getMsgBody().isEmpty()) {
                view.setUpSMS("1 day Ago", bean1Day);
            }

        } else {
            view.emptyList();
        }
    }
}
