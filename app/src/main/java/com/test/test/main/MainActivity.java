package com.test.test.main;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.test.test.R;
import com.test.test.SMSAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MainContract.View{

    private static final int SMS_REQUEST = 100;
    private static final int SETTING_REQUEST = 101;

    public static final String TIME_KEY = "SMS_TIME";

    long smsTime = 0;

    @BindView(R.id.tvEmpty)
    TextView tvEmpty;

    @BindView(R.id.llSMS)
    LinearLayout llSMS;

    MainContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        presenter = new MainPresenter(this);

        if (getIntent() != null && getIntent().getStringExtra(TIME_KEY) != null) {
            smsTime = Long.parseLong(getIntent().getStringExtra(TIME_KEY));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS}, SMS_REQUEST);
            } else {
                presenter.getSMS();
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == SMS_REQUEST) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                presenter.getSMS();
            } else {
                boolean showRationale = shouldShowRequestPermissionRationale(permissions[0]);
                if (showRationale) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this)
                            .setTitle(getResources().getString(R.string.mandatory))
                            .setMessage(getResources().getString(R.string.msg))
                            .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(new String[]{Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS}, SMS_REQUEST);
                                    }
                                    dialog.dismiss();
                                }
                            });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                } else {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivityForResult(intent, SETTING_REQUEST);
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == SETTING_REQUEST) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS}, SMS_REQUEST);
            }
        }
    }

    @Override
    public void emptyList() {
        tvEmpty.setVisibility(View.VISIBLE);
        tvEmpty.setText(getResources().getString(R.string.empty));
    }

    @Override
    public void setUpSMS(String header, SMSBean bean) {
        tvEmpty.setVisibility(View.GONE);
        TextView tvHeader = new TextView(MainActivity.this);
        tvHeader.setText(header);
        tvHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP, 21);
        llSMS.addView(tvHeader);

        RecyclerView rvSMS = new RecyclerView(MainActivity.this);
        SMSAdapter adapter = new SMSAdapter(bean, smsTime);
        LinearLayoutManager manager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
        rvSMS.setLayoutManager(manager);
        rvSMS.setAdapter(adapter);
        llSMS.addView(rvSMS);
    }

    @Override
    public Context getContext() {
        return MainActivity.this;
    }
}
