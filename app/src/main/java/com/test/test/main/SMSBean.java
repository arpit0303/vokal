package com.test.test.main;

import java.util.ArrayList;
import java.util.List;

public class SMSBean {

    private List<String> msgBody = new ArrayList<>();
    private List<String> msgAddress = new ArrayList<>();
    private List<Long> msgTime = new ArrayList<>();

    public List<String> getMsgBody() {
        return msgBody;
    }

    public void setMsgBody(List<String> msgBody) {
        this.msgBody = msgBody;
    }

    public List<String> getMsgAddress() {
        return msgAddress;
    }

    public void setMsgAddress(List<String> msgAddress) {
        this.msgAddress = msgAddress;
    }

    public List<Long> getMsgTime() {
        return msgTime;
    }

    public void setMsgTime(List<Long> msgTime) {
        this.msgTime = msgTime;
    }
}
