package com.test.test.main;

import android.content.Context;

public interface MainContract {

    interface View {
        void emptyList();

        void setUpSMS(String header, SMSBean bean);

        Context getContext();
    }

    interface Presenter {
        void getSMS();
    }
}
